package com.dunadan.app.MoveInReady;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.view.View;

import android.widget.AdapterView.OnItemClickListener;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dunadan.app.MoveInReady.adapter.CustomListAdapter;
import com.dunadan.app.MoveInReady.app.AppController;
import com.dunadan.app.MoveInReady.model.Question;

import com.dunadan.app.MoveInReady.model.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {
    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();

    // Movies json url
    private static final String url = "http://10.0.3.2:3000/articles"; // this is where the db will tie in
    private ProgressDialog pDialog;
    private List<Question> questionList = new ArrayList<Question>();
    private ListView listView;
    private CustomListAdapter adapter;
    private SearchView mSearchView;

    private String user_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.icon);

        Intent intent = getIntent();
        user_id = intent.getStringExtra("user_id");

        listView = (ListView) findViewById(R.id.list);
        adapter = new CustomListAdapter(this, questionList);
        if(adapter != null) {
            listView.setAdapter(adapter);
        }

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String questionId = questionList.get(i).getId();
                Intent myIntent = new Intent(MainActivity.this, ViewQuestionActivity.class);
                myIntent.putExtra("question_id", questionId);
                myIntent.putExtra("user_id", user_id);
                MainActivity.this.startActivity(myIntent);
                finish();
                adapterView.getItemAtPosition(i);
            }
        });


        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();

        // changing action bar color
//        getActionBar().setBackgroundDrawable(
//                new ColorDrawable(Color.parseColor("#1b1b1b")));


        // prepare the Request
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Log.d(TAG, response.toString());
                        hidePDialog();

                        JSONArray res = new JSONArray();

                        try {
                            res = response.getJSONArray("articles");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Parsing json
                        for (int i = 0; i < res.length(); i++) {
                            try {

                                JSONObject obj = res.getJSONObject(i);
                                Question question = new Question();
                                question.setTitle(obj.getString("title"));
                                //question.setThumbnailUrl(obj.getString("image"));
                                question.setId(obj.getString("_id"));

                                JSONObject user = obj.getJSONObject("user");
                                question.setUser(new User(user.getString("_id"), user.getString("username"),user.getString("name")));

                                question.setDate(obj.getString("createdAt"));

                                // Genre is json array
                                JSONArray tagArry = obj.getJSONArray("tags");
                                ArrayList<String> tags = new ArrayList<String>();
                                for (int j = 0; j < tagArry.length(); j++) {
                                    tags.add((String) tagArry.get(j));
                                }
                                question.setTags(tags);

                                // adding question to movies array
                                questionList.add(question);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        hidePDialog();

                    }
                }
        );

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(getRequest);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        mSearchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()){
            case R.id.action_search:
                mSearchView.setIconified(false);
                return true;
            case R.id.action_post_question:
                Intent i = new Intent(getApplicationContext(), PostQuestionActivity.class);
                i.putExtra("user_id", user_id);
                startActivity(i);
        }

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

}

