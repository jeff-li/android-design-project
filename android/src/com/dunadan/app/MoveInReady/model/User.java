package com.dunadan.app.MoveInReady.model;

/**
 * Created by Jeff on 15-01-31.
 */
public class User {
    private String id;
    private String salt;
    private String hashed_password;
    private String username;
    private String email;
    private String name;

    public User(String id, String salt, String hashed_password, String username, String email, String name) {
        this.id = id;
        this.salt = salt;
        this.hashed_password = hashed_password;
        this.username = username;
        this.email = email;
        this.name = name;
    }

    public User(String id, String username, String name) {
        this.id = id;
        this.username = username;
        this.name = name;
    }

    public User() {

    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
}
