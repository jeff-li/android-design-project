package com.dunadan.app.MoveInReady.model;

import java.util.ArrayList;

/**
 * Created by Jeff on 15-01-31.
 */
public class Question {
    private String id;
    private User user;
    private int v;
    private String date;
    private ArrayList<Answer> answers;
    private ArrayList<String> tags;
    private String body;
    private String title;

    public Question() {
    }

    public Question(String id, User user, int v, String date, ArrayList<Answer> answers, ArrayList<String> tags, String body, String title) {
        this.id = id;
        this.user = user;
        this.v = v;
        this.date = date;
        this.answers = answers;
        this.tags = tags;
        this.body = body;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public int getV() {
        return v;
    }

    public String getDate() {
        return date;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public String getBody() {
        return body;
    }

    public String getTitle() {
        return title;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setV(int v) {
        this.v = v;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
