package com.dunadan.app.MoveInReady.model;

import java.util.ArrayList;

/**
 * Created by Tyler on 3/15/2015.
 */
public class Answer {
    private User user;
    private String id;
    private String date;
    private String body;

    public Answer() {
    }

    public Answer(User user, String id, String date, String body) {
        this.user = user;
        this.id = id;
        this.date = date;
        this.body = body;
    }

    public User getUser() {
        return user;
    }

    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getBody() {
        return body;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setBody(String body) {
        this.body = body;
    }
}