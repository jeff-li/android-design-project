package com.dunadan.app.MoveInReady;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dunadan.app.MoveInReady.adapter.CommentListAdapter;
import com.dunadan.app.MoveInReady.adapter.CustomListAdapter;
import com.dunadan.app.MoveInReady.app.AppController;
import com.dunadan.app.MoveInReady.model.Answer;
import com.dunadan.app.MoveInReady.model.Question;

import com.dunadan.app.MoveInReady.model.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ViewQuestionActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {
    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();

    // Movies json url
    private static final String url = "http://10.0.3.2:3000/articles/"; // this is where the db will tie in

    private String id;
    private ProgressDialog pDialog;
    private List<Answer> commentList = new ArrayList<Answer>();
    private ListView listView;
    private TextView questionTitle;
    private TextView questionContent;
    private Button btnAnswer;
    private CommentListAdapter adapter;
    private SearchView mSearchView;

    private String questionId = "";
    private String userId = "";

    private Question question;

    //TODO: https://stackoverflow.com/questions/3913592/start-an-activity-with-a-parameter setup ID to be passed in

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        userId = intent.getStringExtra("user_id");
        questionId = intent.getStringExtra("question_id");

        setContentView(R.layout.activity_question);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.icon);

        listView = (ListView) findViewById(R.id.answerList);
        questionTitle = (TextView) findViewById(R.id.question_title);
        questionContent = (TextView) findViewById(R.id.question_content);
        btnAnswer = (Button) findViewById(R.id.btnAnswer);

        adapter = new CommentListAdapter(this, commentList);
        if(adapter != null) {
            listView.setAdapter(adapter);
        }

        btnAnswer.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent myIntent = new Intent(ViewQuestionActivity.this, PostAnswerActivity.class);
                myIntent.putExtra("question_id", questionId);
                myIntent.putExtra("user_id", userId);
                myIntent.putExtra("question_text", questionContent.getText().toString());
                ViewQuestionActivity.this.startActivity(myIntent);
                finish();
            }
        });

        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();

        // changing action bar color
//        getActionBar().setBackgroundDrawable(
//                new ColorDrawable(Color.parseColor("#1b1b1b")));


        // prepare the Request
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url + questionId, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Log.d(TAG, response.toString());
                        hidePDialog();

                        JSONArray ans = new JSONArray();
                        JSONObject q = new JSONObject();

                        try {
                             q = response.getJSONObject("article");

                            Question question = new Question();
                            //question.setThumbnailUrl(obj.getString("image"));
                            question.setId(q.getString("_id"));

                            JSONObject user = q.getJSONObject("user");
                            question.setUser(new User(user.getString("_id"), user.getString("username"),user.getString("name")));

                            question.setDate(q.getString("createdAt"));
                            String body = q.getString("body");
                            String title = q.getString("title");
                            question.setBody(body);
                            question.setTitle(title);

                            // Genre is json array
                            JSONArray tagArry = q.getJSONArray("tags");
                            ArrayList<String> tags = new ArrayList<String>();
                            for (int j = 0; j < tagArry.length(); j++) {
                                tags.add((String) tagArry.get(j));
                            }
                            question.setTags(tags);

                            ViewQuestionActivity.this.question = question;

                            questionTitle.setText(title);
                            questionContent.setText(body);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {

                            ans = q.getJSONArray("comments");

                            // Parsing json
                            for (int i = 0; i < ans.length(); i++) {

                                JSONObject obj = ans.getJSONObject(i);
                                Answer comment = new Answer();
                                comment.setBody(obj.getString("body"));
                                //question.setThumbnailUrl(obj.getString("image"));
                                comment.setId(obj.getString("_id"));

                                JSONObject user = obj.getJSONObject("user");
                                comment.setUser(new User(user.getString("_id"), user.getString("username"), user.getString("name")));

                                comment.setDate(obj.getString("createdAt"));

                                // adding question to movies array
                                commentList.add(comment);


                            }
                        }catch (JSONException e) {
                                e.printStackTrace();
                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        hidePDialog();

                    }
                }
        );

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(getRequest);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_question, menu);

        MenuItem searchItem = menu.findItem(R.id.action_back);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()){
            case R.id.action_back:
                Intent myIntent = new Intent(ViewQuestionActivity.this, MainActivity.class);
                myIntent.putExtra("user_id", userId);
                ViewQuestionActivity.this.startActivity(myIntent);
                finish();
        }

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

}

