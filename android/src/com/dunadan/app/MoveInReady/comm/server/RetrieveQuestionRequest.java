package com.dunadan.app.MoveInReady.comm.server;

import com.dunadan.app.MoveInReady.model.Question;
import com.github.kevinsawicki.http.HttpRequest;
//import com.google.gson.Gson;

import java.util.List;

/**
 * Created by idrees on 2015-03-18.
 */
public class RetrieveQuestionRequest extends AbstractServerRequest{
    private String id;
    public RetrieveQuestionRequest(String id, List<String> params, List<String> values) {
        super(params, values);
        this.id = id;
    }

    @Override
    public String getPath() {
        return "/articles" + this.id;
    }

    public HttpRequest sendRequest() {
        return super.getRequest();
    }

//    public Question toModel() {
//        Gson gson = new Gson();
//        Question arr = gson.fromJson(response, Question.class);
//        return arr;
//    }
}
