package com.dunadan.app.MoveInReady.comm.server;

import com.github.kevinsawicki.http.HttpRequest;

import java.util.List;

/**
 * Created by idrees on 2015-01-09.
 */
public class TestRequest extends AbstractServerRequest {

    public TestRequest() {
        super();
    }

    public TestRequest(List<String> params, List<String> values) {
        super(params, values);
    }

    @Override
    public String getPath() {
        return "/";
    }

    @Override
    public HttpRequest sendRequest() {
        return null;
    }
}