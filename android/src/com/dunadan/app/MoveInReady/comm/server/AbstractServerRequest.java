package com.dunadan.app.MoveInReady.comm.server;

import com.github.kevinsawicki.http.HttpRequest;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by idrees on 2015-01-07.
 */
public abstract class AbstractServerRequest {
    protected ServerConfig config = ServerConfig.getInstance();
    private List<String> params;
    private List<String> values;
    protected String response;

    protected AbstractServerRequest() {
        this.params = null;
        this.values = null;
    }

    protected AbstractServerRequest(List<String> params, List<String> values) {
        this.params = params;
        this.values = values;
    }

    public List<String> getParams() {
        return params;
    }

    public List<String> getValues() {
        return values;
    }

    protected Map<String,String> createMap(List<String> params, List<String> values) {
        Map<String,String> map = new HashMap<String, String>();
        Iterator<String> i1 = params.iterator();
        Iterator<String> i2 = values.iterator();

        while(i1.hasNext() && i2.hasNext()) {
            map.put(i1.next(), i2.next());
        }
        return map;
    }

    public abstract String getPath();

    public String getCompletePath() {
        return "http://" + config.SERVER_ADDRESS + ":" + config.SERVER_PORT + getPath();
    }

    protected HttpRequest getRequest() {
        HttpRequest response = HttpRequest.get(this.getCompletePath(), createMap(this.params, this.values), true);
        this.response = response.body();
        return response;
    }

    protected HttpRequest postRequest() {
        HttpRequest response = HttpRequest.post(this.getCompletePath(), createMap(this.params, this.values), true);
        this.response = response.body();
        return response;
    }

    public abstract HttpRequest sendRequest();

    //TODO: Add an abstract verification for required parameters
}