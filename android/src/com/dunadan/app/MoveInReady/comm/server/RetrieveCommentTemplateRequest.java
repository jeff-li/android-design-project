package com.dunadan.app.MoveInReady.comm.server;

import com.dunadan.app.MoveInReady.model.Answer;
import com.github.kevinsawicki.http.HttpRequest;
//import com.google.gson.Gson;

import java.util.List;

/**
 * Created by idrees on 2015-03-18.
 */
public class RetrieveCommentTemplateRequest extends AbstractServerRequest {

    private String id;
    public RetrieveCommentTemplateRequest(String id, List<String> params, List<String> values) {
        super(params, values);
        this.id = id;
    }

    @Override
    public String getPath() {
        return "/articles/" + id + "/comments";
    }

    public HttpRequest sendRequest() {
        return super.getRequest();
    }

//    public Answer toModel() {
//        Gson gson = new Gson();
//        Answer arr = gson.fromJson(response, Answer.class);
//        return arr;
//    }
}
