package com.dunadan.app.MoveInReady.comm.server;

import com.github.kevinsawicki.http.HttpRequest;

import java.util.List;

/**
 * Created by idrees on 2015-03-18.
 */
public class QuestionEditRequest extends AbstractServerRequest {

    private String id;

    public QuestionEditRequest(String id, List<String> params, List<String> values) {
        super(params, values);
        this.id = id;
    }

    @Override
    public HttpRequest sendRequest() {
        return super.getRequest();
    }

    @Override
    public String getPath() {
        return "/articles/" + id + "/edit";
    }
}
