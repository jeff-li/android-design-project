package com.dunadan.app.MoveInReady.comm.server;

import com.github.kevinsawicki.http.HttpRequest;

import java.util.List;

/**
 * Created by idrees on 2015-03-09.
 */
public class ResponseCreateRequest extends AbstractServerRequest {
    private String questionId;
    public ResponseCreateRequest() {
        super();
    }

    public ResponseCreateRequest(String questionId, List<String> params, List<String> values) {
        super(params, values);
    }

    @Override
    public String getPath() {
        return "/articles/" + questionId + "/comments";
    }

    @Override
    public HttpRequest sendRequest() {
        return super.postRequest();
    }
}
