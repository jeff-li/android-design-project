package com.dunadan.app.MoveInReady.comm.server;

import com.github.kevinsawicki.http.HttpRequest;

import java.util.List;

/**
 * Created by idrees on 2015-03-09.
 */
public class QuestionCreateRequest extends AbstractServerRequest {
    public QuestionCreateRequest() {
        super();
    }

    public QuestionCreateRequest(List<String> params, List<String> values) {
        super(params, values);
    }

    @Override
    public String getPath() {
        return "/articles";
    }

    @Override
    public HttpRequest sendRequest() {
        return super.postRequest();
    }
}
