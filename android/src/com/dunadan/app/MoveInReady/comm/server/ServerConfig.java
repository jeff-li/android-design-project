package com.dunadan.app.MoveInReady.comm.server;

/**
 * Created by idrees on 2015-01-07.
 */
public class ServerConfig {
    public final String SERVER_ADDRESS = "api.idreeskhan.com";
    public final int SERVER_PORT = 3000;
    private static ServerConfig instance = null;

    protected ServerConfig() {

    }

    public static synchronized ServerConfig getInstance() {
        if (instance == null)
            instance = new ServerConfig();
        return instance;
    }
}