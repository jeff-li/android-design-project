package com.dunadan.app.MoveInReady;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.content.Intent;

/**
 * Created by Jeff on 14-11-27.
 */
public class SplashScreenActivity extends Activity {

    private final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                Intent openLoginActivity =  new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(openLoginActivity);
                finish();

            }
        }, SPLASH_DISPLAY_LENGTH);
    }

}
