package com.dunadan.app.MoveInReady;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.widget.*;
import android.view.View;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dunadan.app.MoveInReady.app.AppController;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Tyler on 1/31/2015.
 */
public class PostAnswerActivity extends Activity {
    private static final String TAG = PostAnswerActivity.class.getSimpleName();
    String msg = "Android: ";

    private TextView questionText = null;
    private EditText answerText = null;
    private Button btnAnswerQuestion = null;
    private Button btnCancel = null;

    private ProgressDialog pDialog;
    private String userId = "";
    private String questionId = "";
    private String qText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_question);

        Intent intent = getIntent();
        userId = intent.getStringExtra("user_id");
        questionId = intent.getStringExtra("question_id");
        qText = intent.getStringExtra("question_text");

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        questionText = (TextView) findViewById(R.id.question_body);
        answerText = (EditText)findViewById(R.id.answer_body);
        btnAnswerQuestion = (Button)findViewById(R.id.btnAnswerQuestion);
        btnCancel = (Button)findViewById(R.id.answer_btnCancel);

        questionText.setText(qText);

        btnAnswerQuestion.setOnClickListener(new View.OnClickListener() { //double check this
            public void onClick(View v) {
                String answer = answerText.getText().toString();

                if (!answer.isEmpty()) {
                    answerQuestion(answer);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter an answer!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent myIntent = new Intent(PostAnswerActivity.this, ViewQuestionActivity.class);
                myIntent.putExtra("question_id", questionId);
                myIntent.putExtra("user_id", userId);
                PostAnswerActivity.this.startActivity(myIntent);
                finish();
            }
        });
    }

    private void answerQuestion(final String answer) {
        // Tag used to cancel the request
        String tag_string_req = "req_post_q";

        pDialog.setMessage("Posting ...");
        showDialog();

        String url = "http://10.0.3.2:3000/articles/" + questionId + "/" + userId + "/comments";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Post Question Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        Toast.makeText(getApplicationContext(), "Answer Posted!", Toast.LENGTH_LONG).show();
                        String questionId = jObj.getString("question_id");
                        // Launch login activity

                        Intent myIntent = new Intent(PostAnswerActivity.this, ViewQuestionActivity.class);
                        myIntent.putExtra("question_id", questionId);
                        myIntent.putExtra("user_id", userId);
                        PostAnswerActivity.this.startActivity(myIntent);
                        finish();
                    } else {
                        JSONArray msgs = jObj.getJSONArray("error");
                        String errorMsg = msgs.toString();
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Answer Question Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("body", answer);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onStart() {
        super.onStart();

        Log.d(msg, "The onStart() event");
    }

    @Override
    public void onRestart() {
        super.onRestart();

        Log.d(msg, "The onRestart() event");
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d(msg, "The onPause() event");
    }

    @Override
    public void onResume() {
        super.onResume();

        /* from this activity, we just want to return to our previous ViewQuestion */

        Log.d(msg, "The onResume() event");
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.d(msg, "The onStop() event");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(msg, "The onDestroy() event");
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}