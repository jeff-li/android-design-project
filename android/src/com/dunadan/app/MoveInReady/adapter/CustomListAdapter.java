package com.dunadan.app.MoveInReady.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import com.dunadan.app.MoveInReady.R;
import com.dunadan.app.MoveInReady.app.AppController;
import com.dunadan.app.MoveInReady.model.Question;

import java.util.List;

/**
 * Created by Jeff on 15-01-27.
 */
public class CustomListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Question> questionItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public CustomListAdapter(Activity activity, List<Question> questionItems) {
        this.activity = activity;
        this.questionItems = questionItems;
    }

    @Override
    public int getCount() {
        return questionItems.size();
    }

    @Override
    public Object getItem(int location) {
        return questionItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.question_list_single, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView date = (TextView) convertView.findViewById(R.id.date);
        TextView genre = (TextView) convertView.findViewById(R.id.tag);
        TextView year = (TextView) convertView.findViewById(R.id.user);

        // getting question data for the row
        Question q = questionItems.get(position);

        // thumbnail image
        thumbNail.setImageUrl("https://www.darujme.sk/data/redactor/39c45a834730b7d6306d75e6c13a1fc0.jpg", imageLoader);

        // title
        title.setText(q.getTitle());

        // date
        date.setText("Date: " + String.valueOf(q.getDate()));

        // tags
        String tagStr = "Tags: ";
        for (String str : q.getTags()) {
            tagStr += str + ", ";
        }
        tagStr = tagStr.length() > 0 ? tagStr.substring(0,
                tagStr.length() - 2) : tagStr;
        genre.setText(tagStr);

        // release year
        year.setText(String.valueOf(q.getUser().getUsername()));

        return convertView;
    }
}

