package com.dunadan.app.MoveInReady.util.server;

import com.github.kevinsawicki.http.HttpRequest;

import java.util.List;

/**
 * Created by idrees on 2015-01-07.
 */
public abstract class AbstractServerRequest {
    protected ServerConfig config = ServerConfig.getInstance();
    private List<String> params;
    private List<String> values;

    protected AbstractServerRequest() {
        this.params = null;
        this.values = null;
    }

    protected AbstractServerRequest(List<String> params, List<String> values) {
        this.params = params;
        this.values = values;
    }

    public List<String> getParams() {
        return params;
    }

    public List<String> getValues() {
        return values;
    }

    public abstract String getPath();
    public abstract String getCompletePath();
    public abstract HttpRequest sendRequest();
}
