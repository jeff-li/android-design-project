package com.dunadan.app.MoveInReady;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.dunadan.app.MoveInReady.R;

/**
 * Created by Tyler on 3/17/2015.
 */
public class ProfileViewActivity extends Activity {

    private TextView Username = null;
    private TextView Profession = null;
    private TextView Company = null;
    private TextView Region = null;
    private ImageView ProfilePic = null;
    private TextView ProfileType = null;
    private TextView ContributorStatus = null;
    //private TextView TotalPoints = null;
    //private TextView TotalQuestionsAnswered = null;
    //private TextView HighestRatedAnswer = null;
    private TextView PointCount = null;
    private TextView QuestionAnsCount = null;
    private TextView HighPointCount = null;
    private EditText AboutMeBody = null;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        Username = (TextView)findViewById(R.id.textView);
        Profession = (TextView)findViewById(R.id.textView2);
        Company = (TextView)findViewById(R.id.textView4);
        Region = (TextView)findViewById(R.id.textView3);
        ProfilePic = (ImageView)findViewById(R.id.imageView);
        ProfileType = (TextView)findViewById(R.id.textView5);
        ContributorStatus = (TextView)findViewById(R.id.textView6);

        PointCount = (TextView)findViewById(R.id.textView11);
        QuestionAnsCount = (TextView)findViewById(R.id.textView12);
        HighPointCount = (TextView)findViewById(R.id.textView13);

        AboutMeBody = (EditText)findViewById(R.id.editText);

        //TODO: need to load the TextViews from the db, can hardcode for now
        //TODO: need to add the abilitity to use teh edit text, button maybe???

    }
}
