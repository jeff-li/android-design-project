package com.dunadan.app.MoveInReady;


import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Spinner;
import com.dunadan.app.MoveInReady.ViewQuestionActivity;
import android.widget.Toast;

import android.app.ProgressDialog;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.dunadan.app.MoveInReady.app.AppController;


public class PostQuestionActivity extends ActionBarActivity {
    private static final String TAG = PostQuestionActivity.class.getSimpleName();

    private EditText questionTitle =null;
    private EditText questionBody =null;
    private Button btnPostQuestion =null;
    private Button btnCancel =null;
    private Spinner questionTags = null;
    private ProgressDialog pDialog;
    private String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_question);

        Intent intent = getIntent();
        userId = intent.getStringExtra("user_id");

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.icon);

        questionTitle = (EditText)findViewById(R.id.ask_question_title);
        questionBody = (EditText)findViewById(R.id.ask_question_body);
        questionTags = (Spinner)findViewById(R.id.ask_question_category_spinner);
        btnPostQuestion = (Button)findViewById(R.id.btnPostQuestion);
        btnCancel = (Button)findViewById(R.id.ask_btnCancel);

        /*Spinner QuestionCategory = (Spinner)findViewById(R.id.ask_question_category_spinner);
        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                QuestionCategory) */ //we not need this since the way Jeff did it should just populate from xml

        btnPostQuestion.setOnClickListener(new View.OnClickListener() { //double check this
            public void onClick(View v) {
                String title = questionTitle.getText().toString();
                String body = questionBody.getText().toString();
                String tags = questionTags.getSelectedItem().toString();

                if (!title.isEmpty() && !title.isEmpty() && !body.isEmpty() && !body.isEmpty()) {
                    postQuestion(title, body, tags);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter question details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent myIntent = new Intent(PostQuestionActivity.this, MainActivity.class);
                myIntent.putExtra("user_id", userId);
                PostQuestionActivity.this.startActivity(myIntent);
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_post_question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Function to store user in MySQL database will post params(tag, name,
     * email, password) to register url
     */
    private void postQuestion(final String title, final String body, final String tags) {
        // Tag used to cancel the request
        String tag_string_req = "req_post_q";

        pDialog.setMessage("Posting ...");
        showDialog();

        String url = "http://10.0.3.2:3000/articles/" + userId;
        StringRequest strReq = new StringRequest(Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Post Question Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        Toast.makeText(getApplicationContext(), "Question Posted!", Toast.LENGTH_LONG).show();

                        JSONObject q = jObj.getJSONObject("question");
                        String questionId = q.getString("_id");
                        // Launch login activity

                        Intent myIntent = new Intent(PostQuestionActivity.this, ViewQuestionActivity.class);
                        myIntent.putExtra("question_id", questionId);
                        myIntent.putExtra("user_id", userId);
                        PostQuestionActivity.this.startActivity(myIntent);
                        finish();
                    } else {
                        JSONArray msgs = jObj.getJSONArray("error");
                        String errorMsg = msgs.toString();
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Post Question Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("title", title);
                params.put("body", body);
                params.put("tags", tags);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}

