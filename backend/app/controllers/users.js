/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var User = mongoose.model('User');
var utils = require('../../lib/utils');

/**
 * Load
 */

exports.load = function (req, res, next, id) {
    var options = {
        criteria: {_id: id}
    };
    User.load(options, function (err, user) {
        if (err) return next(err);
        if (!user) return next(new Error('Failed to load User ' + id));
        req.profile = user;
        next();
    });
};

/**
 * Create user
 */

exports.create = function (req, res) {
    var user = new User(req.body);
    user.provider = 'local';
    user.save(function (err) {
        if (err) {
            res.setHeader('Content-Type', 'application/json');
            return res.end(JSON.stringify({
                error: true,
                error_msg: utils.errors(err.errors),
                user: user,
                title: 'Sign up'
            }));
        }

        res.setHeader('Content-Type', 'application/json');
        return res.end(JSON.stringify({
            error: false,
            user: user
        }));

        // manually login the user once successfully signed up
        //req.logIn(user, function (err) {
        //    if (err) {
        //        res.setHeader('Content-Type', 'application/json');
        //        return res.end(JSON.stringify({
        //            error: utils.errors(err.errors),
        //            info: 'Sorry! We are not able to log you in!'
        //        }));
        //    }
        //
        //    res.setHeader('Content-Type', 'application/json');
        //    return res.end(JSON.stringify({
        //        success: 'success'
        //    }));
        //});
    });
};

/**
 *  Show profile
 */

exports.show = function (req, res) {
    var user = req.profile;
    res.render('users/show', {
        title: user.name,
        user: user
    });
};

exports.signin = function (req, res) {
};

/**
 * Auth callback
 */

exports.authCallback = login;

/**
 * Show login form
 */

exports.login = function (req, res) {
    res.render('users/login', {
        title: 'Login'
    });
};

/**
 * Show sign up form
 */

exports.signup = function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({
        title: 'Sign up',
        user: new User()
    }));
};

/**
 * Logout
 */

exports.logout = function (req, res) {
    req.logout();
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({
        success: 'success'
    }));
};

/**
 * Session
 */

exports.session = login;

/**
 * Login
 */

function login(req, res) {
    var redirectTo = req.session.returnTo ? req.session.returnTo : '/';
    delete req.session.returnTo;
    res.redirect(redirectTo);
};
