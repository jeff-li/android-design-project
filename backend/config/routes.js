/*!
 * Module dependencies.
 */

// Note: We can require users, articles and other cotrollers because we have
// set the NODE_PATH to be ./app/controllers (package.json # scripts # start)

var users = require('users');
var articles = require('articles');
var comments = require('comments');
var tags = require('tags');
var auth = require('./middlewares/authorization');

/**
 * Route middlewares
 */

var articleAuth = [auth.requiresLogin, auth.article.hasAuthorization];
var commentAuth = [auth.requiresLogin, auth.comment.hasAuthorization];

/**
 * Expose routes
 */

module.exports = function (app, passport) {

    // user routes
    app.get('/login', users.login);
    app.get('/signup', users.signup);
    app.get('/logout', users.logout);
    app.post('/users', users.create);
    app.post('/users/login',
        passport.authenticate('local',  { session: false }),
        function(req, res) {
            res.json({  error:false, user: req.user});
        });
    app.get('/users/:userId', users.show);
    //app.get('/auth/facebook',
    //  passport.authenticate('facebook', {
    //    scope: [ 'email', 'user_about_me'],
    //    failureRedirect: '/login'
    //  }), users.signin);
    //app.get('/auth/facebook/callback',
    //  passport.authenticate('facebook', {
    //    failureRedirect: '/login'
    //  }), users.authCallback);

    app.param('userId', users.load);

    // article routes
    app.param('id', articles.load);
    app.get('/articles', articles.index);
    app.get('/articles/new', articles.new);
    app.post('/articles/:userId', articles.create);
    app.get('/articles/:id', articles.show);
    app.get('/articles/:id/edit', articles.edit);
    app.put('/articles/:id', articles.update);
    app.delete('/articles/:id', articles.destroy);

    // home route
    app.get('/', articles.index);

    // comment routes
    app.param('commentId', comments.load);
    app.post('/articles/:id/:userId/comments', comments.create);
    //app.get('/articles/:id/comments', comments.create);
    app.delete('/articles/:id/comments/:commentId', comments.destroy);

    // tag routes
    app.get('/tags/:tag', tags.index);


    /**
     * Error handling
     */

    app.use(function (err, req, res, next) {
        // treat as 404
        if (err.message
            && (~err.message.indexOf('not found')
            || (~err.message.indexOf('Cast to ObjectId failed')))) {
            return next();
        }
        console.error(err.stack);
        // error page
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({
            status: 500,
            error: err.stack
        }));
    });

    // assume 404 since no middleware responded
    app.use(function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({
            status: 404,
            url: req.originalUrl,
            error: 'Not found'
        }));
    });
}
